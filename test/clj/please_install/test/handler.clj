(ns please-install.test.handler
  (:require
    [clojure.test :refer :all]
    [ring.mock.request :refer :all]
    [please-install.handler :refer :all]
    [please-install.middleware.formats :as formats]
    [muuntaja.core :as m]
    [mount.core :as mount]))

(defn parse-json [body]
  (m/decode formats/instance "application/json" body))

(use-fixtures
  :once
  (fn [f]
    (mount/start #'please-install.config/env
                 #'please-install.handler/app-routes)
    (f)))

(deftest test-app
  (testing "main route"
    (let [response ((app) (request :get "/"))]
      (is (= 200 (:status response)))))

  (testing "not-found route"
    (let [response ((app) (request :get "/invalid"))]
      (is (= 404 (:status response))))))

(deftest test-admin-page-routes
  (testing "admin-home"
    (let [response ((app) (request :get "/admin-home"))]
      (is (= 403 (:status response)))))
  
  (testing "admin-users"
    (let [response ((app) (request :get "/admin-users"))]
      (is (= 403 (:status response)))))
  
  (testing "admin-software"
    (let [response ((app) (request :get "/admin-software"))]
      (is (= 403 (:status response)))))
  
  (testing "admin-add-user"
    (let [response ((app) (request :get "/admin-add-user"))]
      (is (= 403 (:status response)))))
  
  (testing "admin-add-software"
    (let [response ((app) (request :get "/admin-add-software"))]
      (is (= 403 (:status response))))))

  (testing "admin-add-user"
    (let [response ((app) (request :post "/admin-add-user"))]
      (is (= 403 (:status response)))))
  
  (testing "admin-add-software"
    (let [response ((app) (request :post "/admin-add-software"))]
      (is (= 403 (:status response))))) 
  
  (testing "admin-del-user"
    (let [response ((app) (request :get "/admin-del-user"))]
      (is (= 403 (:status response)))))
  
  (testing "admin-del-software"
    (let [response ((app) (request :get "/admin-del-software"))]
      (is (= 403 (:status response)))))

  (testing "admin-del-user"
    (let [response ((app) (request :post "/admin-del-user"))]
      (is (= 403 (:status response)))))
  
  (testing "admin-del-software"
    (let [response ((app) (request :post "/admin-del-software"))]
      (is (= 403 (:status response))))) 
  
