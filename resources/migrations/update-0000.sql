-- Necessary postgres extentions
create extension if not exists "uuid-ossp";
create extension if not exists "pgcrypto";

-- This function generates the ammended uuid's that will be used for id's accross the schema
create or replace function uuidgen(count_in integer) returns setof character
    language plpgsql
    as '
  declare
    i int;
  begin
    for i in 1 .. count_in loop
      return next cast(lower(replace(cast(uuid_generate_v4() as varchar(36)), ''-'', '''')) as char(32));
    end loop;

    return;
  end;
';

-- Drops any tables that might exist before table creation
drop view if exists software_report_src;
drop view if exists v_software_categories;
drop table if exists software_report;
drop table if exists distribution_software_install_candidate_map;
drop table if exists package_manager_software_install_candidate_map;
drop table if exists update_version;
drop table if exists distribution_package_manager_xref;
drop table if exists software;
drop table if exists software_sub_category;
drop table if exists software_category;
drop table if exists users;
drop table if exists license;
drop table if exists distribution;
drop table if exists package_manager;

create table users (
  id character(32) not null,
  user_name character varying(32) not null,
  email character varying(128),
  password character(98) not null, -- The App's bcrypt hash length
  internal boolean not null,
  api_key character varying(128),
  modified_user_id character(32),
  modified_date timestamp without time zone not null
);
alter table only users add constraint pk_users_idx primary key (id);
alter table only users add constraint uk_users_1_idx unique (user_name);
alter table only users add constraint uk_users_2_idx unique (api_key);
create index fk_users_1_idx on users using btree (modified_user_id);
alter table only users add constraint fk_users_1 foreign key (modified_user_id) references users(id) on delete cascade deferrable;

create table license (
  id numeric(2,0) not null,
  name character varying(64) not null,
  description character varying(128),
  modified_user_id character(32) not null,
  modified_date timestamp without time zone not null
);
alter table only license add constraint pk_license_idx primary key (id);
alter table only license add constraint uk_license_idk unique (name);
create index fk_license_1_idx on license using btree(modified_user_id);

create table distribution (
  id character(32) not null,
  name character varying(128) not null,
  base character varying(128) not null,
  logo_url character varying(128) not null,
  modified_user_id character(32) not null,
  modified_date timestamp without time zone not null
);
alter table only distribution add constraint pk_distribution_idx primary key (id);
alter table only distribution add constraint uk_distribution_idk unique (name);
create index fk_distribution_1_idx on distribution using btree(modified_user_id);

create table package_manager (
  id character(32) not null,
  name character varying(128) not null,
  modified_user_id character(32) not null,
  modified_date timestamp without time zone not null
);
alter table only package_manager add constraint pk_package_manager_idx primary key (id);
alter table only package_manager add constraint uk_package_manager_idk unique (name);
create index fk_package_manager_1_idx on package_manager using btree(modified_user_id);

create table software_category (
  id character(32) not null,
  name character varying(64) not null,
  description character varying(128),
  modified_user_id character(32) not null,
  modified_date timestamp without time zone not null
);
alter table only software_category add constraint pk_software_category_idx primary key (id);
alter table only software_category add constraint uk_software_category_idk unique (name);
create index fk_software_category_1_idx on license using btree(modified_user_id);

create table software_sub_category (
  id character(32) not null,
  software_category_id character(32),
  name character varying(64) not null,
  description character varying(128),
  modified_user_id character(32) not null,
  modified_date timestamp without time zone not null
);
alter table only software_sub_category add constraint pk_software_sub_category_idx primary key (id);
alter table only software_sub_category add constraint uk_software_sub_category_idk unique (name);
create index fk_software_sub_category_1_idx on software_sub_category using btree(modified_user_id);
alter table only software_sub_category add constraint fk_software_sub_category_1 foreign key (software_category_id) references software_category(id) on delete cascade deferrable;

create table software (
  id character(32) not null,
  name character varying(128) not null,
  license_id numeric(2,0),
  software_category_id character(32), 
  software_sub_category_id character(32),
  description character varying(256),
  main_url character varying(256),
  source_code_url character varying(256),
  cli boolean not null,
  modified_user_id character(32) not null,
  modified_date timestamp without time zone not null
);
alter table only software add constraint pk_software_idx primary key (id);
alter table only software add constraint uk_software_idk unique (name);
alter table only software add constraint fk_software_1 foreign key (license_id) references license(id) on delete set null;
alter table only software add constraint fk_software_2 foreign key (software_category_id) references software_category(id) on delete set null;
alter table only software add constraint fk_software_3 foreign key (software_sub_category_id) references software_sub_category(id) on delete set null;
create index fk_software_1_idx on software using btree(license_id);
create index fk_software_2_idx on software using btree(software_category_id);
create index fk_software_3_idx on software using btree(software_sub_category_id);
create index fk_software_4_idx on software using btree(modified_user_id);

create table distribution_package_manager_xref (
  distribution_id character(32) not null,
  package_manager_id character(32) not null
);
alter table only distribution_package_manager_xref add constraint fk_distribution_package_manager_xref_1 foreign key (distribution_id) references distribution(id) on delete cascade deferrable;
alter table only distribution_package_manager_xref add constraint fk_distribution_package_manager_xref_2 foreign key (package_manager_id) references package_manager(id) on delete cascade deferrable;

create table package_manager_software_install_candidate_map (
  package_manager_id character(32) not null,
  software_id character(32) not null,
  install_candidate character varying(64) not null,
  repo character varying(256)
);
alter table only package_manager_software_install_candidate_map add constraint fk_package_manager_software_install_candidate_map_1 foreign key (package_manager_id) references package_manager(id) on delete cascade deferrable;
alter table only package_manager_software_install_candidate_map add constraint fk_package_manager_software_install_candidate_map_2 foreign key (software_id) references software(id) on delete cascade deferrable;

create table distribution_software_install_candidate_map (
  distribution_id character(32) not null,
  software_id character(32) not null,
  install_candidate character varying(64) not null
);
alter table only distribution_software_install_candidate_map add constraint fk_distribution_software_install_candidate_map_1 foreign key (distribution_id) references distribution(id) on delete cascade deferrable;
alter table only distribution_software_install_candidate_map add constraint fk_distribution_software_install_candidate_map_2 foreign key (software_id) references software(id) on delete cascade deferrable;


-- This table is for keeping track of database updates
create table update_version ( 
  version numeric not null
);
insert into update_version(version) values (0);

create or replace view v_software_categories as (
  select
    sc.name as software_category_name,
    sc.id as software_category_id,
    ssc.name as software_sub_category_name,
    ssc.id as software_sub_category_id
  from 
    software_category sc
    left join software_sub_category ssc on ssc.software_category_id = sc.id
);

create table software_report (
  id character(32) not null,
  software_id character(32) not null,
  software_name character varying(128) not null,
  software_category_id character(32), 
  software_sub_category_id character(32), 
  description character varying(256),
  main_url character varying(256),
  source_code_url character varying(256),
  cli boolean not null,
  license character varying(64) not null,
  install_candidate_aggregate character varying(2048)
);
alter table only software_report add constraint pk_software_report_idx primary key (id);

create or replace view software_report_src as (
  with software_aggregate as (
    select 
      dsicm.software_id,
      '[' || array_to_string(array_agg('{' || '"' || d.id || '":' || '"' || dsicm.install_candidate || '"' || '}'), ',') || ']' as install_candidate_aggregate
    from 
      distribution_software_install_candidate_map dsicm
      inner join distribution d on d.id = dsicm.distribution_id
    group by 
      software_id
  )
  select 
    uuidgen(1),
    s.id,
    s.name as software_name,
    s.software_category_id,
    s.software_sub_category_id, 
    s.description,
    s.main_url,
    s.source_code_url,
    s.cli,
    l.name as license,
    sa.install_candidate_aggregate
  from 
    software s
    inner join software_aggregate sa on sa.software_id = s.id 
    inner join license l on l.id = s.license_id
);

insert into users(id, user_name, email, password, internal, api_key, modified_user_id, modified_date) -- Insert the root user. This user is strictly used for setup purposes
  values ('00000000000000000000000000000000', 'root', null, 'bcrypt+sha512$3f442a472d4b6150645367566b597033$12$9eb6ba1c5f7760589418268e0a7ca2d201b334f6acc3bb3e', true, null, '00000000000000000000000000000000', current_timestamp);

insert into license (id, name, description, modified_user_id, modified_date) values (0, 'Proprietary', null, '00000000000000000000000000000000', current_timestamp);
insert into license (id, name, description, modified_user_id, modified_date) values (1, 'Apache', null, '00000000000000000000000000000000', current_timestamp);
insert into license (id, name, description, modified_user_id, modified_date) values (2, 'BSD', null, '00000000000000000000000000000000', current_timestamp);
insert into license (id, name, description, modified_user_id, modified_date) values (3, 'GPL', null, '00000000000000000000000000000000', current_timestamp);
insert into license (id, name, description, modified_user_id, modified_date) values (4, 'MIT', null, '00000000000000000000000000000000', current_timestamp);
insert into license (id, name, description, modified_user_id, modified_date) values (5, 'Mozilla', null, '00000000000000000000000000000000', current_timestamp);
insert into license (id, name, description, modified_user_id, modified_date) values (6, 'Eclipse', null, '00000000000000000000000000000000', current_timestamp);

insert into distribution (id, name, base, logo_url, modified_user_id, modified_date) values ('660b6e51225244aebec11d96a8b2e263', 'Ubuntu', 'Ubuntu', '/img/logos/ubuntu.svg', '00000000000000000000000000000000', current_timestamp);
insert into distribution (id, name, base, logo_url, modified_user_id, modified_date) values ('5b5fbe3829c643829f2747e147e19cd5', 'Debian', 'Debian', '/img/logos/debian.svg', '00000000000000000000000000000000', current_timestamp);
insert into distribution (id, name, base, logo_url, modified_user_id, modified_date) values ('6ffdb4dc8d8a460d878d4e59af05eb2e', 'Pop!_OS', 'Ubuntu', '/img/logos/popos.svg', '00000000000000000000000000000000', current_timestamp);
insert into distribution (id, name, base, logo_url, modified_user_id, modified_date) values ('a37209e9de154f8b84293933ea4b1a92', 'Arch', 'Arch', '/img/logos/arch.svg', '00000000000000000000000000000000', current_timestamp);
insert into distribution (id, name, base, logo_url, modified_user_id, modified_date) values ('bb63ab32635349d2bdae2457895df17b', 'Manjaro', 'Arch', '/img/logos/manjaro.svg', '00000000000000000000000000000000', current_timestamp);
insert into distribution (id, name, base, logo_url, modified_user_id, modified_date) values ('504c2e0186214c0f86096d9b000fedb6', 'CentOS', 'Red Hat', '/img/logos/centos.svg', '00000000000000000000000000000000', current_timestamp);
insert into distribution (id, name, base, logo_url, modified_user_id, modified_date) values ('ff14f1d42e704a6fa195b3e53eb0a7ae', 'Fedora', 'Red Hat', '/img/logos/fedora.svg', '00000000000000000000000000000000', current_timestamp);

insert into software_category (id, name, description, modified_user_id, modified_date) values ('651e6e7266774096a7112090b6d6b57b', 'Web Browsers', null, '00000000000000000000000000000000', '2020-09-09 19:46:20');
insert into software_category (id, name, description, modified_user_id, modified_date) values ('6370e591e82940b09c03caea1193fe8a', 'Media', null, '00000000000000000000000000000000', '2020-09-09 19:46:20');
insert into software_category (id, name, description, modified_user_id, modified_date) values ('acbe188bd43641369046668a4d8994c0', 'Programming', null, '00000000000000000000000000000000', '2020-09-09 19:46:20');
insert into software_category (id, name, description, modified_user_id, modified_date) values ('0b143ae523f647b5a4c1db8bea90ae35', 'Design', null, '00000000000000000000000000000000', '2020-09-09 19:46:20');
insert into software_category (id, name, description, modified_user_id, modified_date) values ('11f2304a8f3743cbb40490e8c10c4c2b', 'Messaging', null, '00000000000000000000000000000000', '2020-09-09 19:46:20');
insert into software_category (id, name, description, modified_user_id, modified_date) values ('9f5fbb91c4b0489ea22ad8494706a8c0', 'Utilities', null, '00000000000000000000000000000000', '2020-09-09 19:46:20');
insert into software_category (id, name, description, modified_user_id, modified_date) values ('81cd42d216e5487a978d7e3478cad225', 'Documents', null, '00000000000000000000000000000000', '2020-09-09 19:46:20');
insert into software_category (id, name, description, modified_user_id, modified_date) values ('bfa7e59f2a2a4718bfc07885dedb25f7', 'File Sharing', null, '00000000000000000000000000000000', '2020-09-09 19:46:20');

insert into software_sub_category (id, software_category_id, name, description, modified_user_id, modified_date) values ('fb933e701ef34e07b9689dd62c8a5818', '6370e591e82940b09c03caea1193fe8a', 'Video', null, '00000000000000000000000000000000', '2020-09-09 19:46:20');
insert into software_sub_category (id, software_category_id, name, description, modified_user_id, modified_date) values ('fa1ef4d19c724393ab644049995075f4', '6370e591e82940b09c03caea1193fe8a', 'Audio', null, '00000000000000000000000000000000', '2020-09-09 19:46:20');
insert into software_sub_category (id, software_category_id, name, description, modified_user_id, modified_date) values ('1a2f70fb4d9a4940b5c6cfa34843b59c', '6370e591e82940b09c03caea1193fe8a', 'Gaming', null, '00000000000000000000000000000000', '2020-09-09 19:46:20');
insert into software_sub_category (id, software_category_id, name, description, modified_user_id, modified_date) values ('8d53bad90a874e2897ed619198bcfcc3', 'acbe188bd43641369046668a4d8994c0', 'Text Editor', null, '00000000000000000000000000000000', '2020-09-09 19:46:20');
insert into software_sub_category (id, software_category_id, name, description, modified_user_id, modified_date) values ('6f5ed346857146f19a4f12971401b7db', 'acbe188bd43641369046668a4d8994c0', 'IDE', null, '00000000000000000000000000000000', '2020-09-09 19:46:20');
insert into software_sub_category (id, software_category_id, name, description, modified_user_id, modified_date) values ('275f101176f24fe0b54accd44c655af7', 'acbe188bd43641369046668a4d8994c0', 'Languages/Runtimes/Compilers', null, '00000000000000000000000000000000', '2020-09-09 19:46:20');
insert into software_sub_category (id, software_category_id, name, description, modified_user_id, modified_date) values ('e6b6ec1306324009abd22197fcbfabf2', '0b143ae523f647b5a4c1db8bea90ae35', 'Image Editing', null, '00000000000000000000000000000000', '2020-09-09 19:46:20');
insert into software_sub_category (id, software_category_id, name, description, modified_user_id, modified_date) values ('29a715a6ff3f49c59d509a6bb341b10a', '11f2304a8f3743cbb40490e8c10c4c2b', 'IRC', null, '00000000000000000000000000000000', '2020-09-09 19:46:20');
insert into software_sub_category (id, software_category_id, name, description, modified_user_id, modified_date) values ('1b75b3e6bd6744b08adabc49dbeb99f9', '11f2304a8f3743cbb40490e8c10c4c2b', 'Email', null, '00000000000000000000000000000000', '2020-09-09 19:46:20');
insert into software_sub_category (id, software_category_id, name, description, modified_user_id, modified_date) values ('3dabe7f79ac8484f95e852742f7bf87b', '11f2304a8f3743cbb40490e8c10c4c2b', 'Conferencing', null, '00000000000000000000000000000000', '2020-09-09 19:46:20');
insert into software_sub_category (id, software_category_id, name, description, modified_user_id, modified_date) values ('b87f6f50ed004f8c94db1176246dc268', '11f2304a8f3743cbb40490e8c10c4c2b', 'Messaging Other', null, '00000000000000000000000000000000', '2020-09-09 19:46:20');
insert into software_sub_category (id, software_category_id, name, description, modified_user_id, modified_date) values ('1e0d17f6d6c1451f88b4f80188536b0c', 'bfa7e59f2a2a4718bfc07885dedb25f7', 'Bittorrent', null, '00000000000000000000000000000000', '2020-09-09 19:46:20');
insert into software_sub_category (id, software_category_id, name, description, modified_user_id, modified_date) values ('ed30c96e949542c2b22ca8394ed6848f', 'bfa7e59f2a2a4718bfc07885dedb25f7', 'FTP', null, '00000000000000000000000000000000', '2020-09-09 19:46:20');
insert into software_sub_category (id, software_category_id, name, description, modified_user_id, modified_date) values ('10b17a9bf309431881a578103b72b142', '651e6e7266774096a7112090b6d6b57b', 'Web Browsers', 'Web Browsers', '00000000000000000000000000000000', '2020-09-09 20:37:38');
insert into software_sub_category (id, software_category_id, name, description, modified_user_id, modified_date) values ('3cbecd79ed094b85837ec7fc5fda67b1', '81cd42d216e5487a978d7e3478cad225', 'Office', 'Office Suites', '00000000000000000000000000000000', '2020-09-09 20:50:12');
insert into software_sub_category (id, software_category_id, name, description, modified_user_id, modified_date) values ('63e3ed195e4147278cc2ae6005335391', '9f5fbb91c4b0489ea22ad8494706a8c0', 'Virtualization', 'Virtualization', '00000000000000000000000000000000', '2020-09-10 20:35:07');

insert into software (id, name, license_id, software_category_id, software_sub_category_id, description, main_url, source_code_url, cli, modified_user_id, modified_date) values ('e3f54fbd00074738a78e674cae1b3504', 'Vim', 3, 'acbe188bd43641369046668a4d8994c0', '8d53bad90a874e2897ed619198bcfcc3', 'Vi Improved', 'https://www.vim.org/', 'https://github.com/vim', true, '00000000000000000000000000000000', '2020-09-09 19:46:20');
insert into software (id, name, license_id, software_category_id, software_sub_category_id, description, main_url, source_code_url, cli, modified_user_id, modified_date) values ('a4b184ad87164a478bf60426c5ecef1a', 'Krita', 3, '0b143ae523f647b5a4c1db8bea90ae35', 'e6b6ec1306324009abd22197fcbfabf2', 'Krita is a professional FREE and open source painting program.', 'https://krita.org/', 'https://invent.kde.org/graphics/krita', false, '00000000000000000000000000000000', '2020-09-09 19:46:20');
insert into software (id, name, license_id, software_category_id, software_sub_category_id, description, main_url, source_code_url, cli, modified_user_id, modified_date) values ('2843ff91e882465fb2b85c1be051b45f', 'Visual Studio Code', 4, 'acbe188bd43641369046668a4d8994c0', '8d53bad90a874e2897ed619198bcfcc3', 'Visual Studio Code is a code editor redefined and optimized for building and debugging modern web and cloud applications. ', 'https://code.visualstudio.com/', 'https://github.com/microsoft/vscode', false, '00000000000000000000000000000000', '2020-09-09 19:46:20');
insert into software (id, name, license_id, software_category_id, software_sub_category_id, description, main_url, source_code_url, cli, modified_user_id, modified_date) values ('6b2310d697cf45f0b3a287ee618e23e1', 'Firefox', 5, '651e6e7266774096a7112090b6d6b57b', '10b17a9bf309431881a578103b72b142', 'A free and open-source web browser developed by the Mozilla Foundation and its subsidiary, the Mozilla Corporation.', 'https://www.mozilla.org/en-US/firefox/', 'https://searchfox.org/mozilla-central/source', false, '00000000000000000000000000000000', '2020-09-09 20:38:44');
insert into software (id, name, license_id, software_category_id, software_sub_category_id, description, main_url, source_code_url, cli, modified_user_id, modified_date) values ('60b7556b1b8342fa919aa7d293a43ecc', 'Chrome', 0, '651e6e7266774096a7112090b6d6b57b', '10b17a9bf309431881a578103b72b142', 'Google Chrome is a cross-platform web browser developed by Google. ', 'https://www.google.com/chrome/', '', false, '00000000000000000000000000000000', '2020-09-09 20:40:59');
insert into software (id, name, license_id, software_category_id, software_sub_category_id, description, main_url, source_code_url, cli, modified_user_id, modified_date) values ('022b3a92e5f8400faa4dd7eaf1306630', 'Chromium', 6, '651e6e7266774096a7112090b6d6b57b', '10b17a9bf309431881a578103b72b142', 'Chromium is a free and open-source software project from Google.', 'https://www.chromium.org/', 'https://github.com/chromium/chromium', false, '00000000000000000000000000000000', '2020-09-09 20:42:12');
insert into software (id, name, license_id, software_category_id, software_sub_category_id, description, main_url, source_code_url, cli, modified_user_id, modified_date) values ('06ca0f4485d346d79367a445ccb0923d', 'Lynx', 3, '81cd42d216e5487a978d7e3478cad225', '10b17a9bf309431881a578103b72b142', 'Lynx is a customizable text-based web browser for use on cursor-addressable character cell terminals', 'https://lynx.browser.org/', 'https://github.com/kurtchen/Lynx', true, '00000000000000000000000000000000', '2020-09-09 20:44:52');
insert into software (id, name, license_id, software_category_id, software_sub_category_id, description, main_url, source_code_url, cli, modified_user_id, modified_date) values ('242eb638e3b3488bbb6c645ea75f51b8', 'LibreOffice', 5, '81cd42d216e5487a978d7e3478cad225', '3cbecd79ed094b85837ec7fc5fda67b1', 'LibreOffice is a free and powerful office suite.', 'https://www.libreoffice.org/about-us/source-code/', 'https://gerrit.libreoffice.org/q/status:open', false, '00000000000000000000000000000000', '2020-09-09 20:52:04');
insert into software (id, name, license_id, software_category_id, software_sub_category_id, description, main_url, source_code_url, cli, modified_user_id, modified_date) values ('a72db0d267b5413ba3ef440b425954eb', 'OpenOffice', 1, '81cd42d216e5487a978d7e3478cad225', '3cbecd79ed094b85837ec7fc5fda67b1', 'The Free and Open Productivity Suite', 'https://www.openoffice.org/', 'https://gitbox.apache.org/repos/asf?p=openoffice.git', false, '00000000000000000000000000000000', '2020-09-09 20:53:09');
insert into software (id, name, license_id, software_category_id, software_sub_category_id, description, main_url, source_code_url, cli, modified_user_id, modified_date) values ('c74faa45624e4e22bc443d61e4fadfde', 'rtorrent', 3, 'bfa7e59f2a2a4718bfc07885dedb25f7', '1e0d17f6d6c1451f88b4f80188536b0c', 'ncurses bittorrent client', 'https://github.com/rakshasa/rtorrent', 'https://github.com/rakshasa/rtorrent', true, '00000000000000000000000000000000', '2020-09-10 20:17:43');
insert into software (id, name, license_id, software_category_id, software_sub_category_id, description, main_url, source_code_url, cli, modified_user_id, modified_date) values ('555849f9ced74a86985f4107c1e07831', 'qbittorrent', 5, 'bfa7e59f2a2a4718bfc07885dedb25f7', '1e0d17f6d6c1451f88b4f80188536b0c', 'The qBittorrent project aims to provide an open-source software alternative to µTorrent.', 'https://www.qbittorrent.org/', 'https://github.com/qbittorrent/qBittorrent', false, '00000000000000000000000000000000', '2020-09-10 20:19:48');
insert into software (id, name, license_id, software_category_id, software_sub_category_id, description, main_url, source_code_url, cli, modified_user_id, modified_date) values ('c8469f7dca364bfba1a6fec071e58b9f', 'transmission', 3, 'bfa7e59f2a2a4718bfc07885dedb25f7', '1e0d17f6d6c1451f88b4f80188536b0c', 'A fast, easy, and free bittorrent client', 'https://transmissionbt.com/', 'https://github.com/transmission/transmission', false, '00000000000000000000000000000000', '2020-09-10 20:22:19');
insert into software (id, name, license_id, software_category_id, software_sub_category_id, description, main_url, source_code_url, cli, modified_user_id, modified_date) values ('def8c0da6e674a0dbaebbdd852eb561b', 'deluge', 3, 'bfa7e59f2a2a4718bfc07885dedb25f7', '1e0d17f6d6c1451f88b4f80188536b0c', 'Deluge is a lightweight, Free Software, cross-platform BitTorrent client.', 'https://deluge-torrent.org/', 'https://git.deluge-torrent.org/deluge', false, '00000000000000000000000000000000', '2020-09-10 20:25:14');
insert into software (id, name, license_id, software_category_id, software_sub_category_id, description, main_url, source_code_url, cli, modified_user_id, modified_date) values ('584d93bdd3bd4d49b4a57089639c670d', 'VirtualBox', 3, '9f5fbb91c4b0489ea22ad8494706a8c0', '63e3ed195e4147278cc2ae6005335391', 'VirtualBox is a powerful x86 and AMD64/Intel64 virtualization product for enterprise as well as home use', 'https://www.virtualbox.org/', 'https://www.virtualbox.org/browser/vbox/trunk', false, '00000000000000000000000000000000', '2020-09-10 20:36:42');
insert into software (id, name, license_id, software_category_id, software_sub_category_id, description, main_url, source_code_url, cli, modified_user_id, modified_date) values ('eee03235f5ec487bb7036d78b3d2f248', 'irssi', 3, '11f2304a8f3743cbb40490e8c10c4c2b', '29a715a6ff3f49c59d509a6bb341b10a', 'Your text mode chatting application since 1999.', 'https://irssi.org/', 'https://github.com/irssi/irssi', true, 'cc8b19d94abd402aacd3f4c81a24d71b', '2020-09-14 15:58:49');
insert into software (id, name, license_id, software_category_id, software_sub_category_id, description, main_url, source_code_url, cli, modified_user_id, modified_date) values ('74832b171c4d4d259cc2004ecff747df', 'docker', 1, '9f5fbb91c4b0489ea22ad8494706a8c0', '63e3ed195e4147278cc2ae6005335391', 'Docker is a tool designed to make it easier to create, deploy, and run applications by using containers ', 'https://www.docker.com/', 'https://github.com/docker', true, 'd1726fd6fc744466957b1b7409d187bc', '2020-10-10 18:32:43');

insert into package_manager (id, name, modified_user_id, modified_date) values ('9b12b3907cfd4d7db783d0e8feaa6f55', 'apt', '00000000000000000000000000000000', '2020-09-10 20:36:42');
insert into package_manager (id, name, modified_user_id, modified_date) values ('94f776b9255a48f1a387efa8ff0ead3d', 'yum', '00000000000000000000000000000000', '2020-09-10 20:36:42');
insert into package_manager (id, name, modified_user_id, modified_date) values ('0728de8645a24b1d86eaf4980ed7a404', 'pacman', '00000000000000000000000000000000', '2020-09-10 20:36:42');
insert into package_manager (id, name, modified_user_id, modified_date) values ('459297fe849040ba9229f486bca4f1d4', 'snappy', '00000000000000000000000000000000', '2020-09-10 20:36:42');
insert into package_manager (id, name, modified_user_id, modified_date) values ('52760a4228884e79bb1179f6b304c120', 'flatpak', '00000000000000000000000000000000', '2020-09-10 20:36:42');

insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('660b6e51225244aebec11d96a8b2e263', 'e3f54fbd00074738a78e674cae1b3504', 'apt-get install vim');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('660b6e51225244aebec11d96a8b2e263', 'a4b184ad87164a478bf60426c5ecef1a', 'apt-get install krita');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('660b6e51225244aebec11d96a8b2e263', '2843ff91e882465fb2b85c1be051b45f', 'apt-get install visual-studio-code');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('660b6e51225244aebec11d96a8b2e263', '6b2310d697cf45f0b3a287ee618e23e1', 'apt-get install firefox');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('660b6e51225244aebec11d96a8b2e263', '60b7556b1b8342fa919aa7d293a43ecc', 'apt-get install chrome');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('660b6e51225244aebec11d96a8b2e263', '022b3a92e5f8400faa4dd7eaf1306630', 'apt-get install chromium');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('660b6e51225244aebec11d96a8b2e263', '06ca0f4485d346d79367a445ccb0923d', 'apt-get install lynx');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('660b6e51225244aebec11d96a8b2e263', '242eb638e3b3488bbb6c645ea75f51b8', 'apt-get install libreoffice');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('660b6e51225244aebec11d96a8b2e263', 'a72db0d267b5413ba3ef440b425954eb', 'apt-get install openoffice');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('660b6e51225244aebec11d96a8b2e263', 'c74faa45624e4e22bc443d61e4fadfde', 'apt-get install rtorrent');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('660b6e51225244aebec11d96a8b2e263', '555849f9ced74a86985f4107c1e07831', 'apt-get install qbittorrent');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('660b6e51225244aebec11d96a8b2e263', 'c8469f7dca364bfba1a6fec071e58b9f', 'apt-get install transmission');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('660b6e51225244aebec11d96a8b2e263', 'def8c0da6e674a0dbaebbdd852eb561b', 'apt-get install deluge');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('660b6e51225244aebec11d96a8b2e263', '584d93bdd3bd4d49b4a57089639c670d', 'apt-get install virtualbox');

insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('5b5fbe3829c643829f2747e147e19cd5', 'e3f54fbd00074738a78e674cae1b3504', 'apt-get install vim');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('5b5fbe3829c643829f2747e147e19cd5', 'a4b184ad87164a478bf60426c5ecef1a', 'apt-get install krita');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('5b5fbe3829c643829f2747e147e19cd5', '2843ff91e882465fb2b85c1be051b45f', 'apt-get install visual-studio-code');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('5b5fbe3829c643829f2747e147e19cd5', '6b2310d697cf45f0b3a287ee618e23e1', 'apt-get install firefox');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('5b5fbe3829c643829f2747e147e19cd5', '60b7556b1b8342fa919aa7d293a43ecc', 'apt-get install chrome');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('5b5fbe3829c643829f2747e147e19cd5', '022b3a92e5f8400faa4dd7eaf1306630', 'apt-get install chromium');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('5b5fbe3829c643829f2747e147e19cd5', '06ca0f4485d346d79367a445ccb0923d', 'apt-get install lynx');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('5b5fbe3829c643829f2747e147e19cd5', '242eb638e3b3488bbb6c645ea75f51b8', 'apt-get install libreoffice');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('5b5fbe3829c643829f2747e147e19cd5', 'a72db0d267b5413ba3ef440b425954eb', 'apt-get install openoffice');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('5b5fbe3829c643829f2747e147e19cd5', 'c74faa45624e4e22bc443d61e4fadfde', 'apt-get install rtorrent');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('5b5fbe3829c643829f2747e147e19cd5', '555849f9ced74a86985f4107c1e07831', 'apt-get install qbittorrent');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('5b5fbe3829c643829f2747e147e19cd5', 'c8469f7dca364bfba1a6fec071e58b9f', 'apt-get install transmission');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('5b5fbe3829c643829f2747e147e19cd5', 'def8c0da6e674a0dbaebbdd852eb561b', 'apt-get install deluge');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('5b5fbe3829c643829f2747e147e19cd5', '584d93bdd3bd4d49b4a57089639c670d', 'apt-get install virtualbox');

insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('a37209e9de154f8b84293933ea4b1a92', 'e3f54fbd00074738a78e674cae1b3504', 'pacman -S install vim');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('a37209e9de154f8b84293933ea4b1a92', 'a4b184ad87164a478bf60426c5ecef1a', 'pacman -S install krita');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('a37209e9de154f8b84293933ea4b1a92', '2843ff91e882465fb2b85c1be051b45f', 'pacman -S install visual-studio-code');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('a37209e9de154f8b84293933ea4b1a92', '6b2310d697cf45f0b3a287ee618e23e1', 'pacman -S install firefox');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('a37209e9de154f8b84293933ea4b1a92', '60b7556b1b8342fa919aa7d293a43ecc', 'pacman -S install chrome');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('a37209e9de154f8b84293933ea4b1a92', '022b3a92e5f8400faa4dd7eaf1306630', 'pacman -S install chromium');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('a37209e9de154f8b84293933ea4b1a92', '06ca0f4485d346d79367a445ccb0923d', 'pacman -S install lynx');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('a37209e9de154f8b84293933ea4b1a92', '242eb638e3b3488bbb6c645ea75f51b8', 'pacman -S install libreoffice');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('a37209e9de154f8b84293933ea4b1a92', 'a72db0d267b5413ba3ef440b425954eb', 'pacman -S install openoffice');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('a37209e9de154f8b84293933ea4b1a92', 'c74faa45624e4e22bc443d61e4fadfde', 'pacman -S install rtorrent');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('a37209e9de154f8b84293933ea4b1a92', '555849f9ced74a86985f4107c1e07831', 'pacman -S install qbittorrent');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('a37209e9de154f8b84293933ea4b1a92', 'c8469f7dca364bfba1a6fec071e58b9f', 'pacman -S install transmission');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('a37209e9de154f8b84293933ea4b1a92', 'def8c0da6e674a0dbaebbdd852eb561b', 'pacman -S install deluge');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('a37209e9de154f8b84293933ea4b1a92', '584d93bdd3bd4d49b4a57089639c670d', 'pacman -S install virtualbox');

insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('bb63ab32635349d2bdae2457895df17b', 'e3f54fbd00074738a78e674cae1b3504', 'pacman -S install vim');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('bb63ab32635349d2bdae2457895df17b', 'a4b184ad87164a478bf60426c5ecef1a', 'pacman -S install krita');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('bb63ab32635349d2bdae2457895df17b', '2843ff91e882465fb2b85c1be051b45f', 'pacman -S install visual-studio-code');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('bb63ab32635349d2bdae2457895df17b', '6b2310d697cf45f0b3a287ee618e23e1', 'pacman -S install firefox');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('bb63ab32635349d2bdae2457895df17b', '60b7556b1b8342fa919aa7d293a43ecc', 'pacman -S install chrome');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('bb63ab32635349d2bdae2457895df17b', '022b3a92e5f8400faa4dd7eaf1306630', 'pacman -S install chromium');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('bb63ab32635349d2bdae2457895df17b', '06ca0f4485d346d79367a445ccb0923d', 'pacman -S install lynx');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('bb63ab32635349d2bdae2457895df17b', '242eb638e3b3488bbb6c645ea75f51b8', 'pacman -S install libreoffice');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('bb63ab32635349d2bdae2457895df17b', 'a72db0d267b5413ba3ef440b425954eb', 'pacman -S install openoffice');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('bb63ab32635349d2bdae2457895df17b', 'c74faa45624e4e22bc443d61e4fadfde', 'pacman -S install rtorrent');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('bb63ab32635349d2bdae2457895df17b', '555849f9ced74a86985f4107c1e07831', 'pacman -S install qbittorrent');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('bb63ab32635349d2bdae2457895df17b', 'c8469f7dca364bfba1a6fec071e58b9f', 'pacman -S install transmission');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('bb63ab32635349d2bdae2457895df17b', 'def8c0da6e674a0dbaebbdd852eb561b', 'pacman -S install deluge');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('bb63ab32635349d2bdae2457895df17b', '584d93bdd3bd4d49b4a57089639c670d', 'pacman -S install virtualbox');

insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('6ffdb4dc8d8a460d878d4e59af05eb2e', 'e3f54fbd00074738a78e674cae1b3504', 'apt install vim');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('6ffdb4dc8d8a460d878d4e59af05eb2e', 'a4b184ad87164a478bf60426c5ecef1a', 'apt install krita');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('6ffdb4dc8d8a460d878d4e59af05eb2e', '2843ff91e882465fb2b85c1be051b45f', 'apt install visual-studio-code');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('6ffdb4dc8d8a460d878d4e59af05eb2e', '6b2310d697cf45f0b3a287ee618e23e1', 'apt install firefox');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('6ffdb4dc8d8a460d878d4e59af05eb2e', '60b7556b1b8342fa919aa7d293a43ecc', 'apt install chrome');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('6ffdb4dc8d8a460d878d4e59af05eb2e', '022b3a92e5f8400faa4dd7eaf1306630', 'apt install chromium');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('6ffdb4dc8d8a460d878d4e59af05eb2e', '06ca0f4485d346d79367a445ccb0923d', 'apt install lynx');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('6ffdb4dc8d8a460d878d4e59af05eb2e', '242eb638e3b3488bbb6c645ea75f51b8', 'apt install libreoffice');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('6ffdb4dc8d8a460d878d4e59af05eb2e', 'a72db0d267b5413ba3ef440b425954eb', 'apt install openoffice');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('6ffdb4dc8d8a460d878d4e59af05eb2e', 'c74faa45624e4e22bc443d61e4fadfde', 'apt install rtorrent');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('6ffdb4dc8d8a460d878d4e59af05eb2e', '555849f9ced74a86985f4107c1e07831', 'apt install qbittorrent');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('6ffdb4dc8d8a460d878d4e59af05eb2e', 'c8469f7dca364bfba1a6fec071e58b9f', 'apt install transmission');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('6ffdb4dc8d8a460d878d4e59af05eb2e', 'def8c0da6e674a0dbaebbdd852eb561b', 'apt install deluge');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('6ffdb4dc8d8a460d878d4e59af05eb2e', '584d93bdd3bd4d49b4a57089639c670d', 'apt install virtualbox');

insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('504c2e0186214c0f86096d9b000fedb6', 'e3f54fbd00074738a78e674cae1b3504', 'yum install vim');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('504c2e0186214c0f86096d9b000fedb6', 'a4b184ad87164a478bf60426c5ecef1a', 'yum install krita');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('504c2e0186214c0f86096d9b000fedb6', '2843ff91e882465fb2b85c1be051b45f', 'yum install visual-studio-code');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('504c2e0186214c0f86096d9b000fedb6', '6b2310d697cf45f0b3a287ee618e23e1', 'yum install firefox');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('504c2e0186214c0f86096d9b000fedb6', '60b7556b1b8342fa919aa7d293a43ecc', 'yum install chrome');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('504c2e0186214c0f86096d9b000fedb6', '022b3a92e5f8400faa4dd7eaf1306630', 'yum install chromium');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('504c2e0186214c0f86096d9b000fedb6', '06ca0f4485d346d79367a445ccb0923d', 'yum install lynx');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('504c2e0186214c0f86096d9b000fedb6', '242eb638e3b3488bbb6c645ea75f51b8', 'yum install libreoffice');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('504c2e0186214c0f86096d9b000fedb6', 'a72db0d267b5413ba3ef440b425954eb', 'yum install openoffice');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('504c2e0186214c0f86096d9b000fedb6', 'c74faa45624e4e22bc443d61e4fadfde', 'yum install rtorrent');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('504c2e0186214c0f86096d9b000fedb6', '555849f9ced74a86985f4107c1e07831', 'yum install qbittorrent');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('504c2e0186214c0f86096d9b000fedb6', 'c8469f7dca364bfba1a6fec071e58b9f', 'yum install transmission');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('504c2e0186214c0f86096d9b000fedb6', 'def8c0da6e674a0dbaebbdd852eb561b', 'yum install deluge');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('504c2e0186214c0f86096d9b000fedb6', '584d93bdd3bd4d49b4a57089639c670d', 'yum install virtualbox');

insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('ff14f1d42e704a6fa195b3e53eb0a7ae', 'e3f54fbd00074738a78e674cae1b3504', 'yum install vim');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('ff14f1d42e704a6fa195b3e53eb0a7ae', 'a4b184ad87164a478bf60426c5ecef1a', 'yum install krita');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('ff14f1d42e704a6fa195b3e53eb0a7ae', '2843ff91e882465fb2b85c1be051b45f', 'yum install visual-studio-code');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('ff14f1d42e704a6fa195b3e53eb0a7ae', '6b2310d697cf45f0b3a287ee618e23e1', 'yum install firefox');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('ff14f1d42e704a6fa195b3e53eb0a7ae', '60b7556b1b8342fa919aa7d293a43ecc', 'yum install chrome');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('ff14f1d42e704a6fa195b3e53eb0a7ae', '022b3a92e5f8400faa4dd7eaf1306630', 'yum install chromium');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('ff14f1d42e704a6fa195b3e53eb0a7ae', '06ca0f4485d346d79367a445ccb0923d', 'yum install lynx');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('ff14f1d42e704a6fa195b3e53eb0a7ae', '242eb638e3b3488bbb6c645ea75f51b8', 'yum install libreoffice');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('ff14f1d42e704a6fa195b3e53eb0a7ae', 'a72db0d267b5413ba3ef440b425954eb', 'yum install openoffice');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('ff14f1d42e704a6fa195b3e53eb0a7ae', 'c74faa45624e4e22bc443d61e4fadfde', 'yum install rtorrent');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('ff14f1d42e704a6fa195b3e53eb0a7ae', '555849f9ced74a86985f4107c1e07831', 'yum install qbittorrent');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('ff14f1d42e704a6fa195b3e53eb0a7ae', 'c8469f7dca364bfba1a6fec071e58b9f', 'yum install transmission');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('ff14f1d42e704a6fa195b3e53eb0a7ae', 'def8c0da6e674a0dbaebbdd852eb561b', 'yum install deluge');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('ff14f1d42e704a6fa195b3e53eb0a7ae', '584d93bdd3bd4d49b4a57089639c670d', 'yum install virtualbox');

insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('660b6e51225244aebec11d96a8b2e263', 'eee03235f5ec487bb7036d78b3d2f248', 'apt install irssi');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('5b5fbe3829c643829f2747e147e19cd5', 'eee03235f5ec487bb7036d78b3d2f248', 'apt install irssi');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('6ffdb4dc8d8a460d878d4e59af05eb2e', 'eee03235f5ec487bb7036d78b3d2f248', 'apt install irssi');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('a37209e9de154f8b84293933ea4b1a92', 'eee03235f5ec487bb7036d78b3d2f248', 'pacman -S install irssi');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('bb63ab32635349d2bdae2457895df17b', 'eee03235f5ec487bb7036d78b3d2f248', 'pacman -S install irssi');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('504c2e0186214c0f86096d9b000fedb6', 'eee03235f5ec487bb7036d78b3d2f248', 'yum install irssi');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('ff14f1d42e704a6fa195b3e53eb0a7ae', 'eee03235f5ec487bb7036d78b3d2f248', 'yum install irssi');

insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('660b6e51225244aebec11d96a8b2e263', '74832b171c4d4d259cc2004ecff747df', 'apt-get install docker.io');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('5b5fbe3829c643829f2747e147e19cd5', '74832b171c4d4d259cc2004ecff747df', 'apt-get install docker.io');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('6ffdb4dc8d8a460d878d4e59af05eb2e', '74832b171c4d4d259cc2004ecff747df', 'apt-get install docker.io');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('504c2e0186214c0f86096d9b000fedb6', '74832b171c4d4d259cc2004ecff747df', 'yum install docker');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('ff14f1d42e704a6fa195b3e53eb0a7ae', '74832b171c4d4d259cc2004ecff747df', 'yum install docker');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('a37209e9de154f8b84293933ea4b1a92', '74832b171c4d4d259cc2004ecff747df', 'pacman -S docker');
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate) values ('bb63ab32635349d2bdae2457895df17b', '74832b171c4d4d259cc2004ecff747df', 'pacman -S docker');



truncate table software_report;
insert into software_report select * from software_report_src;