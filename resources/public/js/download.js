function download(extention) {
  let filename = 'pleaseinstallme.' + extention;
  let text = document.getElementById("software-text-area").value;
  let element = document.createElement('a');
  
  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download', filename);

  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}