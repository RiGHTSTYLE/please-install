function copyTextAreaToClipboard() {
  let textArea = document.getElementById("software-text-area");

  textArea.select();
  textArea.setSelectionRange(0, 99999); /*For mobile devices*/
  
  /* Copy the text inside the text field */
  document.execCommand("copy");
}