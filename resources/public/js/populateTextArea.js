function selectDistro(distro, distroSelected=false) {
    let brands = document.getElementsByClassName('brand-img');
    let selectedBrand = document.getElementById(distro.id);

    for (var i = 0; i < brands.length; i++) {
      brands[i].style.border = '1px solid #ffffff';
      brands[i].setAttribute("is-selected", "false");
    }

    selectedBrand.style.border = '1px solid #ff0000';
    selectedBrand.setAttribute("is-selected", "true");

    if (!distroSelected) {
      document.getElementById("software-text-area").value = '';
      softwares = document.getElementsByClassName("software");
      for (var i = 0; i < softwares.length; i++) {
        softwares[i].style.color = '#000000';
        softwares[i].selected = "false";
      }
    }
}

function select(software) {
    if (software.selected === "true") {
      software.style.color = '#000000';
      software.selected = "false";
      alterList(false, software);
    } else {
      software.style.color = '#9e9e9e';
      software.selected = "true";
      alterList(true, software);
    }
  }

function alterList(add, software) {
  let textArea = document.getElementById("software-text-area");
  let isSudo = document.getElementById("sudo").checked;
  let brands = document.getElementsByClassName('brand-img');
  let brandId = null;
  let distroInstallCandidates = null;
  let installCandidateString = null;
  let fullInstallCandidateString = null;
  let candidates = [];

  for (var i = 0; i < brands.length; i++) {
    if (brands[i].getAttribute("is-selected") === "true") {
      brandId = brands[i].getAttribute("id");
    }
  }

  if (brandId === null) { // This fires if no distros are selected. It selects Ubuntu as default.
    selectDistro(document.getElementById("660b6e51225244aebec11d96a8b2e263"), distroSelected=true);
    brandId = "660b6e51225244aebec11d96a8b2e263";
  }

  distroInstallCandidates = JSON.parse(software.getAttribute("value"));

  distroInstallCandidates.forEach(installCandidate => {
    if (Object.keys(installCandidate)[0] === brandId) { installCandidateString = Object.values(installCandidate)[0]; }
  });

  fullInstallCandidateString = isSudo ? "sudo " + installCandidateString + ";" : installCandidateString + ";";

  if (add) {
    textArea.value = textArea.value === '' ? fullInstallCandidateString : textArea.value + '\n' + fullInstallCandidateString;
  } else {
    textArea.value.split("\n").forEach(line => {
      if (!line.includes(installCandidateString)) { candidates.push(line) }
    });
    textArea.value = candidates.join("\n");
  }
}