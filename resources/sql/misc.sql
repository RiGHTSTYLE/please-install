-- :name get-single-uuid :? :1
-- :doc gets a single uuid
select 
  uuidgen(1) as uuid