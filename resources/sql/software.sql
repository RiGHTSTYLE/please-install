-- :name get-all-software 
-- :doc retrieve all software
select 
  s.* 
from 
  software s

-- :name get-software-by-name :? :1
-- :doc retrieve software by specific name
select 
  s.* 
from 
  software s
where 
  lower(s.name) = lower(:name)

-- :name get-software-by-id :? :1
-- :doc retrieve software by specific id
select 
  s.*
from 
  s.software
where 
  s.id = :id

-- :name get-software-by-software-category-name :? :1
-- :doc retrieve software by software category name
select 
  s.*
from 
  software s
  inner join software_category sc on sc.id = s.software_category_id
where 
  sc.name = :name

-- :name get-software-by-software-category-id :? :1
-- :doc retrieve software by software category id
select 
  s.*
from 
  software s
  inner join software_category sc on sc.id = s.id
where 
  sc.id = :id

-- :name get-admin-software-data
-- :doc retrieve the software data for the software admin page table
select
  s.id,
  s.name as software_name,
  sc.name as software_category_name,
  ssc.name as software_sub_category_name,
  l.name as license_name,
  s.description,
  s.main_url,
  s.source_code_url, 
  s.cli
from 
  software s
  inner join software_category sc on sc.id = s.software_category_id
  inner join software_sub_category ssc on ssc.id = s.software_sub_category_id
  inner join license l on l.id = s.license_id

-- :name add-software :! :n
-- :doc add a new piece of software
insert into software (id, name, license_id, software_category_id, software_sub_category_id, description, main_url, source_code_url, cli, modified_user_id, modified_date)
  values(:id, :name, :license_id, :software_category_id, :software_sub_category_id, :description, :main_url, :source_code_url, :cli, :modified_user_id, current_timestamp)

-- :name delete-software-by-id :! :1
-- :doc deletes a piece of software given the id
delete from software where id = :id

-- :name get-software-report 
-- :doc get the software report
select 
  sr.*
from 
  software_report sr