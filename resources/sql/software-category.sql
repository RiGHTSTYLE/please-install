-- :name get-all-software-categories
-- :doc retrieve all software categories
select 
  sc.*
from 
  software_category sc

-- :name get-software-category-by-software-sub-category-id :? :1
-- :doc get the software category by software sub category id
select 
  sc.*
from 
  software_category sc
  inner join software_sub_category ssc on ssc.software_category_id = sc.id
where
  ssc.id = :id

-- :name add-software-category :! :n
-- :doc add a new software category
insert into software_category (id, name, description, modified_user_id, modified_date)
  values(uuidgen(1), :name, :description, :modified_user_id, current_timestamp)

-- :name delete-software-category-by-id :! :1
-- :doc deletes a software category given the id
delete from software_category where id = :id