-- :name get-all-distributions
-- :doc get all software distributions
select 
  d.*
from 
  distribution d

-- :name add-distribution-software-install-candidate :! :n
-- :doc add a entry for distribution_software_install_candidate_map
insert into distribution_software_install_candidate_map (distribution_id, software_id, install_candidate)
select 
  json_array_elements(:distribution_json::json) ->> 'id' as distribution_id,
  :software_id as software_id,
  json_array_elements(:distribution_json::json) ->> 'val' as install_candidate