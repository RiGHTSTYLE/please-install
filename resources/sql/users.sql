-- :name get-all-users :? :*
-- :doc retrieves all users
select
  u.*
from
  users u

-- :name get-number-of-users
-- :doc retrieve number of users
select 
  count(u.id) 
from 
  users u;

-- :name add-new-user :! :n
-- :doc adds a new user
insert into users(id, user_name, email, password, internal, api_key, modified_user_id, modified_date) 
  values (uuidgen(1), :username, :email, :password, :internal, null, '00000000000000000000000000000000', current_timestamp);

-- :name authenticate-user :? :1
-- :doc returns the user id
select 
  u.*
from 
  users u 
where
  u.user_name =  :username
  and u.password = :password
  
-- :name delete-user-by-id :! :1
-- :doc deletes a user by id
delete from users where id = :id