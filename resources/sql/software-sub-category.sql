-- :name get-all-software-sub-categories
-- :doc retrieve all software categories
select 
  *
from 
  software_sub_category

-- :name add-software-sub-category :! :n
-- :doc add a new software sub category
insert into software_sub_category (id, name, description, software_category_id, modified_user_id, modified_date)
  values(uuidgen(1), :name, :description, :software_category_id, :modified_user_id, current_timestamp)

-- :name delete-software-sub-category-by-id :! :1
-- :doc deletes a software sub category given the id
delete from software_sub_category where id = :id