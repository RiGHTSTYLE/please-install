FROM openjdk:8-alpine

COPY target/uberjar/please-install.jar /please-install/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/please-install/app.jar"]
