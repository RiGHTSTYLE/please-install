(ns please-install.routes.admin
  (:require
   [please-install.layout :as layout]
   [please-install.db.core :as db]
   [please-install.middleware :as middleware]
   [ring.util.response]
   [buddy.hashers :as hashers]))

(defn admin-home [request]
  (layout/render request "admin-pages/admin-users.html" {:users (db/get-all-users)}))

(defn admin-software [request]
  (layout/render request "admin-pages/admin-software.html" {:software-datas (db/get-admin-software-data)
                                                            :software-categories (db/get-all-software-categories)
                                                            :software-sub-categories (db/get-all-software-sub-categories)
                                                            :licenses (db/get-all-licenses)
                                                            :distributions (db/get-all-distributions)}))

(defn admin-add-user [request]
  (db/add-new-user {:username (get-in request [:params :username])
                    :email (get-in request [:params :email])
                    :internal (not (nil? (get-in request [:params :internal])))
                    :password (hashers/derive (get-in request [:params :password]) {:alg :bcrypt+sha512
                                                                                    :salt "?D*G-KaPdSgVkYp3"
                                                                                    :iterations 12})})
  (admin-home request))

(defn admin-del-user [request]
  (db/delete-user-by-id {:id (get-in request [:params :user-id])})
  (admin-home request))

(defn admin-add-software [request]
  (let [software-id (get (db/get-single-uuid) :uuid)]
  (db/add-software {:id software-id
                    :name (get-in request [:params :software_name])
                    :license_id (Integer/parseInt (get-in request [:params :licenses]))
                    :main_url (get-in request [:params :main_url])
                    :source_code_url (get-in request [:params :source_code_url])
                    :software_category_id (get (db/get-software-category-by-software-sub-category-id
                                                {:id (get-in request [:params :software-category-sub-category])}) :id)
                    :software_sub_category_id (get-in request [:params :software-category-sub-category])
                    :cli (not (nil? (get-in request [:params :cli])))
                    :description (get-in request [:params :description])
                    :modified_user_id (get-in request [:identity :id])})
  (db/add-distribution-software-install-candidate {:distribution_json (get-in request [:params :distribution-install-candidate])
                                                   :software_id software-id}))
  
  (admin-software request))

(defn admin-del-software [request]
  (db/delete-software-by-id {:id (get-in request [:params :software-id])})
  (admin-software request))

(defn admin-add-software-category [request]
  (db/add-software-category {:name (get-in request [:params :software-category-name])
                             :description (get-in request [:params :description])
                             :modified_user_id (get-in request [:identity :id])})
  (admin-software request))

(defn admin-del-software-category [request]
  (db/delete-software-category-by-id {:id (get-in request [:params :software-category-id])})
  (admin-software request))

(defn admin-add-software-sub-category [request]
  (db/add-software-sub-category {:name (get-in request [:params :software-sub-category-name])
              :description (get-in request [:params :software-sub-category-name-description])
              :software_category_id (get-in request [:params :software-categories])
              :modified_user_id (get-in request [:identity :id])})
  (admin-software request))

(defn admin-del-software-sub-category [request]
  (db/delete-software-sub-category-by-id {:id (get-in request [:params :software-sub-category-id])})
  (admin-software request))

(defn admin-routes []
  [""
   {:middleware [middleware/wrap-csrf
                 middleware/wrap-formats]}
   ["/admin-home" {:get admin-home :post admin-home}]
   ["/admin-users" {:get admin-home :post admin-home}]
   ["/admin-software" {:get admin-software :post admin-software}]
   ["/admin-add-user" {:post admin-add-user}]
   ["/admin-del-user" {:post admin-del-user}]
   ["/admin-add-software" {:post admin-add-software}]
   ["/admin-del-software" {:post admin-del-software}]
   ["/admin-add-software-category" {:post admin-add-software-category}]
   ["/admin-del-software-category" {:post admin-del-software-category}]
   ["/admin-add-software-sub-category" {:post admin-add-software-sub-category}]
   ["/admin-del-software-sub-category" {:post admin-del-software-sub-category}]])

