(ns please-install.routes.home
  (:require
   [please-install.layout :as layout]
   [please-install.db.core :as db]
   [please-install.middleware :as middleware]
   [ring.util.response]))

(defn home-page [request]
  (layout/render request "home.html" (merge {:software (db/get-all-software)}, 
                                            {:software-categories (db/get-all-software-categories)}, 
                                            {:software-report (db/get-software-report)}
                                            {:distributions (db/get-all-distributions)})))

(defn about-page [request]
  (layout/render request "about.html"))

(defn home-routes []
  [""
   {:middleware [middleware/wrap-csrf
                 middleware/wrap-formats]}
   ["/" {:get home-page}]
   ["/about" {:get about-page}]])