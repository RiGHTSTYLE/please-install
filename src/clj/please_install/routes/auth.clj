(ns please-install.routes.auth
  (:require
   [please-install.layout :as layout]
   [please-install.db.core :as db]
   [please-install.middleware :as middleware]
   [ring.util.response :refer [redirect]]
   [buddy.hashers :as hashers]))

(defn lookup-user [request]
  (db/authenticate-user {:username (get-in request [:params :username])
                         :password (hashers/derive (get-in request [:params :passhash]) {:alg :bcrypt+sha512
                                                                                         :salt "?D*G-KaPdSgVkYp3"
                                                                                         :iterations 12})}))

(defn login-page [request & message]
  (layout/render request "login.html" {:message message}))

(defn do-login [request]
  (if-let [user (lookup-user request)]                             
    (assoc (redirect "/admin-home")                                          
           :session (assoc (get request :session) :identity user)) 
    (login-page request "Incorrect username or password.")))                                          

(defn do-logout [{session :session}]
  (-> (redirect "/login")                           ; Redirect to login
      (assoc :session (dissoc session :identity)))) ; Remove :identity from session

(defn signup-page [request]
  (if (< (get (get (db/get-number-of-users) 0) :count) 2)
    (layout/render request "signup.html")
    (ring.util.response/redirect "/login")))

(defn signup-user [request]
  (if (< (get (get (db/get-number-of-users) 0) :count) 2)
    (db/add-new-user {:username (get-in request [:params :username])
                      :email (get-in request [:params :email])
                      :internal true
                      :password (hashers/derive (get-in request [:params :passhash]) {:alg :bcrypt+sha512
                                                                                      :salt "?D*G-KaPdSgVkYp3"
                                                                                      :iterations 12})})
    (do-login request))
    (redirect "/login"))

(defn auth-routes []
  [""
   {:middleware [middleware/wrap-csrf
                 middleware/wrap-formats]}
   ["/login" {:get login-page
              :post do-login}]
   ["/signup" {:get signup-page
               :post signup-user}]
   ["/logout" {:get do-logout
               :post do-logout}]])

