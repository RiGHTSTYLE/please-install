(ns please-install.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[please-install started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[please-install has shut down successfully]=-"))
   :middleware identity})
