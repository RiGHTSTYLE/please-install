(ns please-install.env
  (:require
    [selmer.parser :as parser]
    [clojure.tools.logging :as log]
    [please-install.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[please-install started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[please-install has shut down successfully]=-"))
   :middleware wrap-dev})
